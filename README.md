# README #

Software utilizzato nel pulpito di comando del Laboratorio di Meccatronica dell'Università di Trento. Programma realizzato per il lavoro di tesi "Progettazione e sviluppo di un pulpito di comando per un manipolatore seriale cartesiano" di Alessandro Grillo, Marzo 2016.

## main.rb
Software principale di interfaccia con la CNC Bosch-Rexroth.

##lcd.rb
Libreria di gestione LCD per controller HD44780.