class Lcd

  # Define some device constants
  $LCD_WIDTH = 20    # Maximum characters per line
  $LCD_CHR = HIGH
  $LCD_CMD = LOW
 
  $LCD_LINE_1 = 0x80 # LCD RAM address for the 1st line
  $LCD_LINE_2 = 0xC0 # LCD RAM address for the 2nd line
  $LCD_LINE_3 = 0x94 # LCD RAM address for the 3rd line
  $LCD_LINE_4 = 0xD4 # LCD RAM address for the 4th line
 
  # Timing constants
  E_PULSE = 0.0005
  E_DELAY = 0.0005
 

  def each_char
      self.split("").each { |i| yield i }
  end
  
  def lcd_init()
    # Initialise display
    lcd_byte(0x33,$LCD_CMD) # 110011 Initialise
    lcd_byte(0x32,$LCD_CMD) # 110010 Initialise

    lcd_byte(0x06,$LCD_CMD) # 000110 Cursor move direction
    lcd_byte(0x0C,$LCD_CMD) # 001100 Display On,Cursor Off, Blink Off
    lcd_byte(0x28,$LCD_CMD) # 101000 Data length, number of lines, font size
    lcd_byte(0x01,$LCD_CMD) # 000001 Clear display
    Timing::delay(E_DELAY)
  
  end

  def lcd_byte(bits, mode)
    # Send byte to data pins
    # bits = data
    # mode = True  for character
    #        False for command
 
    Core::digital_write($LCD_RS, mode) # RS
 
    # High bits
    Core::digital_write($LCD_D4, LOW)
    Core::digital_write($LCD_D5, LOW)
    Core::digital_write($LCD_D6, LOW)
    Core::digital_write($LCD_D7, LOW)
  
    if bits&0x10==0x10 then
      Core::digital_write($LCD_D4, HIGH)
    end
    if bits&0x20==0x20 then
      Core::digital_write($LCD_D5, HIGH)
    end
    if bits&0x40==0x40 then
      Core::digital_write($LCD_D6, HIGH)
    end
    if bits&0x80==0x80 then
      Core::digital_write($LCD_D7, HIGH)
    end
   
    # Toggle 'Enable' pin
    lcd_toggle_enable()
 
    # Low bits
    Core::digital_write($LCD_D4, LOW)
    Core::digital_write($LCD_D5, LOW)
    Core::digital_write($LCD_D6, LOW)
    Core::digital_write($LCD_D7, LOW)
    if bits&0x01==0x01 then
      Core::digital_write($LCD_D4, HIGH)
    end
    if bits&0x02==0x02 then
      Core::digital_write($LCD_D5, HIGH)
    end
    if bits&0x04==0x04 then
      Core::digital_write($LCD_D6, HIGH)
    end
    if bits&0x08==0x08 then
      Core::digital_write($LCD_D7, HIGH)
    end
  
 
    # Toggle 'Enable' pin
    lcd_toggle_enable()
  
  end

  def lcd_toggle_enable()
    # Toggle enable
  
    Timing::delay(E_DELAY) 
    Core::digital_write($LCD_E, HIGH)  
    Timing::delay(E_PULSE)  
    Core::digital_write($LCD_E, LOW)
    Timing::delay(E_DELAY) 
  end

  def clear()
    lcd_byte(0x01, $LCD_CMD)
  end
    
  def lcd_string(message,line,style)
    # Send string to display
    # style=1 Left justified
    # style=2 Centred
    # style=3 Right justified
  

    if style==1 then
      message = message.ljust($LCD_WIDTH)
      #elsif style==2 then
      #message = message.center(LCD_WIDTH)
      #elsif style==3 then
      #message = message.rjust(LCD_WIDTH)
    else
    end
 
    lcd_byte(line, $LCD_CMD)
 
    message.each_char do |lettera|

        n=lettera.ord # trasforma ogni lettera in numero
        lcd_byte( n , $LCD_CHR )
    
    end

  end
    
    
end