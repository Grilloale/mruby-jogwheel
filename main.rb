########################################################################################################################################   
###  main.rb (necessita di includere la libreria lcd.rb , nello stesso folder)
###  Questo programma permette la movimentazione manuale del manipolatore cartesiano Bosch-Rexroth.
###  Istruzioni telecomando:
###  1] Una volta acceso premendo il bottone ON, il programma parte automaticamente (DA FARE)
###  2] Di default la macchina rimane ferma nonstante si muova la rotellina. Per poter muovere un asse è necessario 
###     selezionare il relativo bottone.
###  3] Una volta scelto, utilizzare la manovella per determinare lo spostamento. Ripremere il bottone per confermare e uscire.
###     NB: non è possibile selezionare un altro asse senza aver ripremuto l'asse corrente.
###  4] È possibile determinare in qualsiasi momento lo step di precisione (in mm) con l'apposito bottone.
###  5] Se viene premuto il bottone EMERGENZA, la macchina si disconnette.
###
###  Alessandro Grillo, 9 Novembre 2015.
########################################################################################################################################   

include Raspberry
include Raspberry::Core
include Raspberry::Timing
include Raspberry::Specifics
require "./lcd.rb"                                     #Importo classe LCD

#### Impostazione pin per pulsanti e LCD ####
#Encoder
$pinA=21   # A channel
$pinB=22   # B channel
$start=8
#Buttons
$butE=9
$butX=7
$butY=15 
$butZ=16
$butS=12
$pinBuzz=28 
# LCD - Definisco GPIO per LCD 
$LCD_RS = 1
$LCD_E  = 5
$LCD_D4 = 6
$LCD_D5 = 0 
$LCD_D6 = 2
$LCD_D7 = 3   

#### Impostazione pin INPUT / OUTPUT e resistenze ####
#Encoder
Core::pin_mode($pinA,INPUT) 
Core::pin_mode($pinB,INPUT)     
Core::pull_mode($pinA,PUD_UP)
Core::pull_mode($pinB,PUD_UP)
#Buttons
Core::pin_mode($butX,INPUT) 
Core::pin_mode($butY,INPUT) 
Core::pin_mode($butZ,INPUT) 
Core::pin_mode($butS,INPUT) 
Core::pull_mode($butX,PUD_UP) 
Core::pull_mode($butY,PUD_UP) 
Core::pull_mode($butZ,PUD_UP) 
Core::pull_mode($butS,PUD_UP) 
Core::pin_mode($pinBuzz,OUTPUT)
#LCD
Core::pin_mode($LCD_E,OUTPUT)  # E
Core::pin_mode($LCD_RS,OUTPUT) # RS
Core::pin_mode($LCD_D4,OUTPUT) # DB4 
Core::pin_mode($LCD_D5,OUTPUT) # DB5 
Core::pin_mode($LCD_D6,OUTPUT) # DB6
Core::pin_mode($LCD_D7,OUTPUT) # DB7 

########################################################################################
    
class Machine
 
  @ENABLE_TIME=500e3.to_i # [ms]
    
  attr_accessor :x
  attr_accessor :y
  attr_accessor :z
  attr_accessor :selected
  attr_reader   :error
  attr_reader   :coords
  attr_reader   :postext
  
  def initialize()
    #Creazione degli oggetti: creo i 3 assi.
    @x= EasyIndra::Drive.new()
    @y= EasyIndra::Drive.new()
    @z= EasyIndra::Drive.new()
    @error= EasyIndra::errors?()
    #Dichiarazione indirizzo per ogni asse
    @x.init("192.168.0.101", :position)
    @y.init("192.168.0.102", :position)
    @z.init("192.168.0.103", :position)
    
    @valo=0
    
  end
  
  def connect()
      
    # Inizializzo la libreria easyindra
    puts "- Inizializzo Easyindra -"
    EasyIndra::init_easyindra([@x, @y, @z])
    
    # Procedura di reset, stacco, riconnessione
    puts "- Reset status -"
    @x.connect()
    @y.connect()
    @z.connect()
    EasyIndra::reset_status()
    puts "OK"
    @x.disconnect()
    @y.disconnect()
    @z.disconnect()
    puts "- Connetto i drives -"
    @x.connect()
    @y.connect()
    @z.connect()
    puts "OK"
    puts ""
  end
  
  ## Attivazione drives: ora posso comunicare con la macchina.
  def enable()
    
    puts "- Abilito drives -"
    @x.enable()
    @y.enable()
    @z.enable()
    usleep(@ENABLE_TIME)
    puts "OK"
    puts ""
    
  end
  
  ## Disabilitazione macchina.
  def disable()
    puts "- Disconnetto drives -"
    @x.disable()
    @y.disable()
    @z.disable()
    puts "OK"
    puts "- Disconnessione drives -"
    @x.disconnect()
    @y.disconnect()
    @z.disconnect()
    puts "OK"
    puts ""
    puts "Chiudo la libreria EasyIndra..."
    EasyIndra::shutdown()
    puts "OK"
    puts ""
  end
  
  #posizione attuale
  def position()
     @coords=[@x.get_position, @y.get_position, @z.get_position]
     @postext=[(@coords[0]).to_s, (@coords[1]).to_s, (@coords[2]).to_s ] #Converto i numeri in stringa per visualizzarli su LCD
     
     return @coords
  end
  
  def newpos(n)
    
    case @selected      
      when "x"
        @coords[0]+=(n)
        @valo=(@coords[0]) 
        @x.setpoint(@valo.to_f)    
           
      when "y"
        @coords[1]+=(n)
        @valo=(@coords[1])
        @y.setpoint(@valo.to_f)     
          
      when "z"
        @coords[2]+=(n)
        @valo=(@coords[2])
        @z.setpoint(@valo.to_f)
      when 0
        # non fare nulla
      else
        # daccapo
    end
    
  end
  
  def whatlimits()
    # get.limits() mi da un array [ minimo, massimo]
    # da cui, es:  @limits= [[-5.000,475.000], [-5.000,445.000], [-265.000,5.000]]
    
    @limits=[@x.get_limits(), @y.get_limits(), @z.get_limits()]  
    #Imposto 1 mm di offset per sicurezza
    @limits=[  [(@limits[0][0])+1, (@limits[0][1])-1], [(@limits[1][0])+1, (@limits[1][1])-1], [(@limits[2][0])+1, (@limits[2][1])-1] ]
    return @limits
    
  end
  
  def outoflim()
    
    case @selected      
      when "x"
            
        #Determino se sono dentro il range dei limiti
        if (@limits[0][0])<=@valo and @valo<=(@limits[0][1]) then
          
          return true
        #Se non sono nel range, allora l'asse è fermo - out of range  
        else
          puts "X Out of limits"
          alarm()
          return false
        end
                   
      when "y"
        
        if (@limits[1][0])<=@valo and @valo<=(@limits[1][1]) then
          return true
        else
          puts "Y Out of limits"
          alarm()
          return false
        end  
          
      when "z"
                
        if (@limits[2][0])<=@valo and @valo<=(@limits[2][1]) then
          return true
        else
          puts "Z Out of limits"
          alarm()
          return false
        end  
        
      when 0
        # non fare nulla
      else
        # daccapo
    end
    
  end
  
  def alarm
        #Allarme per fuori-range
        Core::digital_write($pinBuzz, HIGH)
        Timing::delay(600)    
        Core::digital_write($pinBuzz, LOW)
        Timing::delay(600)
  end
  
 
end

########################################################################################

class Tools
  
  #attr_accessor :stato
  attr_accessor :step
  attr_accessor :old_value
  attr_accessor :new_value
  attr_accessor :cont
  attr_accessor :sStart
  attr_accessor :stateE
  attr_reader   :steptext
    
  def initialize()
    @step=1.0.to_f 
    @steptext="1.0"   #di default 1 click= 1 step = 1mm
    @old_value=1
    @new_value=0
    @cont=0  
    
    @stateX=1
    @stateY=1
    @stateZ=1
    @axe=0
    
    @stateE=1
    @sStart=1
    @stateS=1
    @i=0
    @s=1.0
    @ary=[(1.0).to_f,(0.1).to_f]

  end    
  
  def enter 
        #Feedback sonoro singolo
        Core::digital_write($pinBuzz, HIGH)
        Timing::delay(100)    
        Core::digital_write($pinBuzz, LOW)
  end
  
  def exit  
        #Feedback sonoro doppio
        Core::digital_write($pinBuzz, HIGH)
        Timing::delay(100)    
        Core::digital_write($pinBuzz, LOW)
        Timing::delay(100) 
        Core::digital_write($pinBuzz, HIGH)
        Timing::delay(100)    
        Core::digital_write($pinBuzz, LOW)
  end
  
  def rotary()    
    
    A=Core::digital_read($pinA)
    B=Core::digital_read($pinB)

    xor=(A^B)

    #Sono al 1° avvio: sicomme l'encoder è fermo, per solo il caso in cui old_value=1  definito nelle costanti
    # • Metto old_value = new_value
    # • Mi esegue il codice solo 1 volta
    # • Prosegue e non mi fa i confronti, così restituisce un valore 0: sono fermo.

    if @old_value==1 then
      @old_value = ( A*2 + B*3 + xor*1 ) #mi cambia subito il valore da 1 a (0,3,5 or 4) dunque verrà eseguito SOLO all'avvio.
    else 
    end

    @new_value = ( A*2 + B*3 + xor*1 )

    if @new_value!=@old_value then

      if @new_value==0 then
        if @old_value==4 then
          @cont=1
        elsif @old_value==3
          @cont=(-1) 
        else
        end
      end

      if @new_value==3 then
        if @old_value==0 then
          @cont=1          
        elsif @old_value==5
          then 
          @cont=(-1)   
        else
        end
      end

      if @new_value==5 then
        if @old_value==3 then
          @cont=1
        elsif @old_value==4
          then 
          @cont=(-1)   
        else
        end
      end

      if @new_value==4 then
        if @old_value==5 then
          @cont=1
        elsif @old_value==0
          then 
          @cont=(-1)   
        else
        end
      end

    else
      @cont=0
    end

    @old_value = @new_value
    
    return @cont 
      
    #return NON E' necessario, ma è buona cosa rendere chiaro cosa mi ritorna il blocco di programma per una migliore lettura.
    
  end
  
  def axe_button()
    
    Xput=Core::digital_read($butX)            #Bottone premuto= 0,  altrimenti 1
    Yput=Core::digital_read($butY)
    Zput=Core::digital_read($butZ)
  
    # X button
    if Xput==0 && @stateX==1 then             #Premo il bottone: da 1->0
      puts "1) Confermo X"
      enter()
      @stateX=0                               #Update stato: è premuto
      @axe="x"
      Timing::delay(200)

    elsif Xput==1 && @stateX==0 then          #Rilascio il bottone: se prima era premuto then...finchè non lo ripremo.
   
      @axe="x"   
      @stateY=2                               #Disabilito altri pulsanti
      @stateZ=2
      
    elsif Xput==0 && @stateX==0 then          #Ripremo il bottone per confermare.
    
      puts "3) Exit X"
      exit()
      @stateX=1
  
      @stateY=1                               #Riattivo altri pulsanti
      @stateZ=1
      @axe=0                                  #Non sto selezionando nulla.
      Timing::delay(200)
      
    else
    end

    # Y button
    if Yput==0 && @stateY==1 then
      puts "1) Confermo Y"
      enter()
      @stateY=0
      @axe="y"
      Timing::delay(200)

    elsif Yput==1 && @stateY==0 then
      @axe="y"
      @stateX=2  
      @stateZ=2
    
    elsif Yput==0 && @stateY==0 then
      puts "3) Exit Y"
      exit()
      @stateY=1
  
      @stateX=1  
      @stateZ=1
      @axe=0
      Timing::delay(200)
      
    else
    end
  
    # Z button
    if Zput==0 && @stateZ==1 then
      puts "1) Confermo Z"
      enter()
      @stateZ=0
      @axe="z"
      Timing::delay(200)

    elsif Zput==1 && @stateZ==0 then
      @axe="z"
      @stateX=2  
      @stateY=2
      
      
    elsif Zput==0 && @stateZ==0 then
      puts "3) Exit Z"
      exit()
      @stateZ=1
  
      @stateX=1   
      @stateY=1
      @axe=0
      Timing::delay(200)
      
    else
    end
    
    return @axe
    
  end
  
  def axe_step()
    
    Sput=Core::digital_read($butS)           #Bottone premuto= 0, altrimenti 1
  
    @i=0 if @i==2                            #Permette di ritornare all'inizio dell'array una volta superato i=2

    if Sput==0 && @stateS==1 then            #Premo il bottone: da 1->0
      enter()
      @stateS=0                              #Se premuto, aggiorna lo stato di servizio come 0 (premuto)
      @i+=1                                  #Incrementa contatore di 1
                
      @s=@ary[@i]                            #Leggo la posizione i-esima dell'array
      puts "#{@s}"                           #Scrivo la risoluzione scelta
      @steptext="%.1f" % @s
      Timing::delay(200)
    
    elsif Sput==0 && @stateS==0 then         #Ripremo il bottone per cambiare.
      enter()
      @stateS=1
      @i+=1
      
      if @i>=2 then                           #Riporto il contatore all'inizio se con il contatore supero la dimensione
        @i=@i-2
      end
      
      @s=@ary[@i]
      puts "#{@s}"  
      @steptext="%.1f" % @s                   # %.1f indica: 1 cifra dopo la virgola del float; % @s indica di quale float
      Timing::delay(200)
    
    else
      #nulla                                   
    end
    
    return @s                                #Ritorna il valore precedente
    
  end
  
  def start() 
    
    Start=Core::digital_read($start)
    
    if Start==0 && @sStart==1 then              
      @sStart=0                              
      puts " Controller attivato"
    else
      #nulla
    end
        
    return @sStart #0 
  end
  
  def emergency()
    
    Eput=Core::digital_read($butE)
    
    if Eput==0 && @stateE==1 then             #Premo il bottone: da 1->0
      @stateE=0                               #Se premuto, aggiorna lo stato di servizio come 0 (premuto)
      puts " EMERGENZA - STOP"  
      
      #Feedback sonoro 
      Core::digital_write($pinBuzz, HIGH)
      Timing::delay(900)   
      Core::digital_write($pinBuzz, LOW)
      Timing::delay(100)
      
      Core::digital_write($pinBuzz, HIGH)
      Timing::delay(100) 
      Core::digital_write($pinBuzz, LOW)
      Timing::delay(100)
      Core::digital_write($pinBuzz, HIGH)
      Timing::delay(100)
      Core::digital_write($pinBuzz, LOW)
      Timing::delay(100)
      Core::digital_write($pinBuzz, HIGH)
      Timing::delay(100)
      Core::digital_write($pinBuzz, LOW)
      Timing::delay(100)
      Core::digital_write($pinBuzz, HIGH)
      Timing::delay(100)
      Core::digital_write($pinBuzz, LOW)
      Timing::delay(100)
                    
    else
      #nulla
    end
    
    return @stateE #0
  
  end
   
end
########################################################################################

class Main
  
  attr_accessor :assi         
  attr_accessor :encoder      
  
  def initialize()
    @assi=Machine.new()        # Creo un nuovo oggetto "assi" contenente i tre assi.
    @encoder=Tools.new()       # Creo un nuovo oggetto "encoder".
    @tot=0  

  end
  
  def resolution()
    
    @encoder.rotary()                           # Determino @cont,  valore +1 (orario), 0, -1 (antiorario) 
    @tot+=@encoder.cont                         # Parte da 0: aggiungo il valore cui sopra.
    
      if @tot==4 then
        #puts "-->> a { DX }"
        @assi.newpos(@encoder.step)
        @tot=0    
      elsif @tot==(-4) then
        #puts "<<-- a { SX }"
        @assi.newpos((-1)*@encoder.step)
        @tot=0
      else      
      end
      
  end           
   
  def set_step()                               
    @encoder.step=@encoder.axe_step()                         
  end
  
  def set_assi(n)
    @assi.selected=n
  end
  
  def set_button()
    @encoder.axe_button() 
  end

end

########################################################################################   
    
################################################
### =>  PARTE LOGICA: 25 Febbraio 2016
### =>  Alessandro Grillo
################################################

# Creazione oggetti
controller=Main.new();  lcd=Lcd.new()

# Inizializzazione display

lcd.lcd_init()
lcd.clear()
Timing::delay(1000)

#NB: Il refresh dell'LCD avviene secondo gli indici i,k
i=0
k=0

loop do
  
  i+=1 
  if i==500 then
      lcd.lcd_string("     MACHINE is    ",$LCD_LINE_2,1)
      lcd.lcd_string("      DISABLED     ",$LCD_LINE_3,1)
  end

  #Premo TASTO ATTIVAZIONE: ENABLE
  if controller.encoder.start==0 then                 
    
    controller.assi.connect()   # Connessione macchina
    controller.assi.enable()    # Abilitazione macchina
    controller.assi.position()  # Lettura posizione assi
    

    lcd.clear()
    puts "uno"
    
    ########################################################################################   
    
    #Se restituisce False=CONNESSA (NON ci sono errori)
    while controller.assi.error==false 
      
      controller.set_step()                           # Lettura STEP (da bottone): 1.0 or 0.1
      controller.set_assi( controller.set_button() )  # Lettura ASSE (da bottone)
      controller.resolution()                         # Nuovo posizionamento
             
                          
      k+=1
      if k==1100 then
        
        controller.assi.whatlimits()                  # Determino quali sono i limiti impostati alla macchina (in continuazione così non si disconnette)
        controller.assi.outoflim()                    # Verifico se sono fuori dai limiti
        controller.assi.position()                    # Lettura posizione
        
        
        lcd.lcd_string(" X:      #{controller.assi.postext[0]} ",$LCD_LINE_1,1)
        lcd.lcd_string(" Y:      #{controller.assi.postext[1]} ",$LCD_LINE_2,1)
        lcd.lcd_string(" Z:      #{controller.assi.postext[2]} ",$LCD_LINE_3,1)
        lcd.lcd_string("Step:            #{controller.encoder.steptext}",$LCD_LINE_4,1)
        
        k=0
      
      end
      
    ########################################################################################   
        
      #Premo EMERGENCY=STOP BUTTON
      if controller.encoder.emergency==0 then        
      
        controller.assi.disable()                     # Disabilitazione macchina
        puts "Macchina Disabilitata"    
        controller.encoder.sStart=1                   # Reset degli stati dei pulsanti: ora se vengono ripremuto hanno effetto
        controller.encoder.stateE=1
        
        lcd.clear()
        lcd.lcd_string("      EMERGENCY!",$LCD_LINE_2,1)
        lcd.lcd_string("  MACHINE DISABLED",$LCD_LINE_3,1)

        break                                         # ESCO dall'intero ciclo 
      end
    end 
    
    # Se la macchina si è DISCONNESSA (o ho qualsiasi tipo di errore)
    if controller.assi.error==true then               # true = è disconnessa
      controller.encoder.sStart=1                     # Reset degli stati dei pulsanti: ora se vengono ripremuto hanno effetto
      controller.encoder.stateE=1                     # Nel frattempo, il bottone ENABLE è su quindi poi quando lo ripremo riparte il programma...
      
      lcd.clear()      
      lcd.lcd_string("      EMERGENCY!",$LCD_LINE_2,1)
      lcd.lcd_string("  MACHINE DISABLED",$LCD_LINE_3,1)
      
    else
      #nulla
    end
      
     
  else
    #nulla
  end  
  
end
